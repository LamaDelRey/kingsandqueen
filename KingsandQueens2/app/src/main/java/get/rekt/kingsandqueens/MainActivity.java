package get.rekt.kingsandqueens;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView textViewToChange = (TextView) findViewById(R.id.Affichage_test);
        if (isConnected()) {
            textViewToChange.setText("JSON, first batch :");
            try {

                // Here, simply, we do dem good old JSON. Since you made it an array we have to create an array. Logical.
                JSONArray civArray = DataHolder.getInstance().getCivilizations();
                int len = civArray.length();
                final MainActivity self = this;
                for (int i = 0 ; i < len ; i++ )
                {
                    JSONObject civ = civArray.getJSONObject(i);
                    String nameFirstObject = civ.getString("name");
                    LinearLayout mainLayout = (LinearLayout)findViewById(R.id.buttonsLayout);
                    Button myButton = new Button(this);
                    myButton.setText(nameFirstObject);
                    myButton.setId(i);
                    mainLayout.addView(myButton);
                    final int x = i;
                    myButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Intent intent = new Intent(self, CivRecapActivity.class);
                            intent.putExtra("EXTRA_INT", x);
                            startActivity(intent);
                        }
                });

                }

//                textViewToChange.setText("JSON, first array,first case :" + firstObject + "name of the first object is :" + nameFirstObject + "name of the first array of the first array, first case" + gods);
            } catch (Exception e) {
//                textViewToChange.setText("Error Thrown :" + e);
                textViewToChange.setText("You need internet to use correctly this application! " + e);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


}
