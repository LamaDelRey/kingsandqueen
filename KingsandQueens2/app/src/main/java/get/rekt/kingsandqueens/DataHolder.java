package get.rekt.kingsandqueens;

import android.os.AsyncTask;
import android.os.Debug;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.util.concurrent.ExecutionException;

/**
 * Created by salve_000 on 19/06/2015.
 */
public class DataHolder {
    static private DataHolder _instance;
    private JSONArray _civilizations;

    private DataHolder() {
        fetchData();
    }

    private void fetchData() {
        HttpAsyncTask civRequest = new HttpAsyncTask();
        civRequest.execute("https://gist.githubusercontent.com/LamaDelRay/1a28039134d699e8c87e/raw/e70800956f4a61c5d3a02d3b6f4b86eba475432b/aom.json");
        try {
            _civilizations = new JSONArray(civRequest.get());
        } catch (Exception e) {
            System.out.println("error : " + e);
            fetchData();
        }
    }

    static synchronized public DataHolder getInstance() {
        if (_instance == null) {
            _instance = new DataHolder();
        }
        return _instance;
    }

    public JSONObject getCivilization(int i){
        try {
            return _civilizations.getJSONObject(i);
        } catch (JSONException e) {
            return null;
        }
    }

    public JSONArray getCivilizations() {
        return _civilizations;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        public String GET(String url) {
            InputStream inputStream = null;
            String result = "";
            try {
                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                // convert inputstream to string
                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                } else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }
            return result;
        }


        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
    }
}
