package get.rekt.kingsandqueens;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class details extends ActionBarActivity {

    private JSONObject _civilization;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Intent intent = getIntent();
        TextView desc =(TextView) findViewById(R.id.deityDescription);
        TextView nameDe = (TextView) findViewById(R.id.deityNameView);
        int deityID = intent.getIntExtra("EXTRA_INT", 0);
        String type = intent.getStringExtra("EXTRA_STR");
        int civID = intent.getIntExtra("CIV_ID", 0);
        _civilization = DataHolder.getInstance().getCivilization(civID);
        try {
            JSONArray deity = _civilization.getJSONArray(type);
            JSONObject deityObject = deity.getJSONObject(deityID);
            String deityName = deityObject.getString("name");
            String deityDescription = deityObject.getString("description");
            nameDe.setText(deityName);
            desc.setText("Description : " + deityDescription);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
