package get.rekt.kingsandqueens;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CivRecapActivity extends ActionBarActivity {
    private JSONObject _civilization;
    private int _civId;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civ_recap);
        Intent intent = getIntent();
        _civId = intent.getIntExtra("EXTRA_INT", 0);
        _civilization = DataHolder.getInstance().getCivilization(_civId);
        final TextView textViewToChange = (TextView) findViewById(R.id.debugCivRecap);
        final TextView civilizationName = (TextView) findViewById(R.id.civName);
        try {
            String name = _civilization.getString("name");
            civilizationName.setText(name);
            LinearLayout layout = (LinearLayout)findViewById(R.id.civLayout);
            for (String type : new String[]{"gods", "minor_gods"}) {
                addGods(layout, type, _civilization.getJSONArray(type));
            }
//            textViewToChange.setText("Vous avez recup la civilisation : " + name + "Which has the following gods :" + gods);
        } catch (JSONException e) {
            System.out.println("oups: " + e);
            return;
        }
    }

    private void addGods(LinearLayout layout, String type, JSONArray gods) throws JSONException {
        TextView label = new TextView(this);
        label.setText(type);
        layout.addView(label);

        ArrayList<String> godNames = new ArrayList<>();
        for (int i = 0, l = gods.length(); i < l; ++i)
        {
            JSONObject god = gods.getJSONObject(i);
 //           JSONArray godArray = gods.getJSONArray(i);
            String nameGod = " Name : " + god.getString("name");
            String excerptGod = " Short description : " + god.getString("excerpt");
            godNames.add(nameGod + excerptGod);
        }

        // first, generate the layout
        LinearLayout godsLayout = new LinearLayout(this);
        godsLayout.setId(LinearLayout.generateViewId());
        layout.addView(godsLayout);

        // then generate the list
        final ListView godsDetails = new ListView(this);
        godsDetails.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, godNames));
        godsDetails.setId(LinearLayout.generateViewId());
        godsDetails.setClickable(true);

        final CivRecapActivity self = this;
        final String _type = type;
        final int civId = _civId;
        godsDetails.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Object o = godsDetails.getItemAtPosition(position);
                Intent intent = new Intent(self, details.class);
                intent.putExtra("CIV_ID", civId);
                intent.putExtra("EXTRA_INT", position);
                intent.putExtra("EXTRA_STR",_type);
                startActivity(intent);
            }
        });
        godsLayout.addView(godsDetails);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_civ_recap, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
